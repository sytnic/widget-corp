<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>
<?php confirm_logged_in(); ?>
<?php find_selected_page(); 

if (isset($_POST['submit'])) {
	// Process the form     
    
    $sub_id = (int)$_SESSION["sub_id"];
    //$sub_id = $current_subject["id"];
	$menu_name = mysql_prep($_POST["menu_name"]);
	$position = (int)$_POST["position"];
    
    // т.к. (по умолчанию) никакой radiobox может быть не отмечен 
    if (isset($_POST["visible"])) {      
	    $visible = (int)$_POST["visible"];
    } else {
        $_POST["visible"] = "";
        $visible = $_POST["visible"];       
    }
        
    $content = mysql_prep($_POST["content"]);
    
             
    // validations
	$required_fields = array("menu_name", "position", "visible", "content");
	validate_presences($required_fields);
	
	$fields_with_max_lengths = array("menu_name" => 30);
	validate_max_lengths($fields_with_max_lengths);
	
    if (!empty($errors)) {
		$_SESSION["errors"] = $errors;
		redirect_to("new_page.php?subject=".$sub_id);
	}
    
	$query  = "INSERT INTO pages (";
	$query .= "  subject_id, menu_name, position, visible, content";
	$query .= ") VALUES (";
	$query .= "  {$sub_id}, '{$menu_name}', {$position}, {$visible}, '{$content}'";
	$query .= ")";
	$result = mysqli_query($connection, $query);

    // echo $query; // It will cause - Cannot modify header

	if ($result) {
		// Success
		$_SESSION["message"] = "Page created.";
		redirect_to("manage_content.php?subject=".$sub_id);
	} else {
		// Failure
		$_SESSION["message"] = "Page creation failed.";
		redirect_to("manage_content.php");
	}    
    	
} else {
	// Вероятно, GET запрос
	// 
}	
?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>
<?php $_SESSION["sub_id"] = $current_subject["id"];  ?>
<div id="main">
  <div id="navigation">
    <?php 
	// навигация в аргументах получает null или ассоц. массив 
	echo navigation($current_subject, $current_page); ?>
	
  </div>
  <div id="page">
        <?php echo message();   ?>   
        <?php $errors = errors(); ?>
		<?php echo form_errors($errors); ?>        
                   
        <h2>Create Page for "<?php echo $current_subject["menu_name"]; ?>"</h2>
		
	    <form action="new_page.php" method="post">        
        
		  <p>Menu name:
		    <input type="text" name="menu_name" value="" />
		  </p>
		  <p>Position:
		    <select name="position">
            <?php                
                $page_set = find_all_pages_for_subject($current_subject["id"], false);
                    // true означает WHERE visible = 1
				$page_count = mysqli_num_rows($page_set);
                
                for($count=1; $count <= $page_count + 1; $count++) {
                   echo "<option value=\"{$count}\">{$count}</option>";
               }
            ?>				
		    </select>
		  </p>
		  <p>Visible:
		    <input type="radio" name="visible" value="0" /> No
		    &nbsp;
		    <input type="radio" name="visible" value="1" /> Yes
		  </p>
          Content: <br>
          <textarea name="content" rows="18" cols="75"></textarea>
          <br><br>
		  <input type="submit" name="submit" value="Create Page" />
		</form>
		<br />
		<a href="manage_content.php?subject=<?php echo urlencode($current_subject["id"]); ?>">Back</a>	
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>