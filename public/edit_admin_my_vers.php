<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>
<?php 
    if (isset($_GET["id"])) {
        $admin = find_admin_by_id($_GET["id"]);
    } else {
        $admin = null;
        redirect_to("manage_admins.php");
    }

    if (isset($_POST['submit'])) {
        // Process the form     
            
        $username = mysql_prep($_POST["username"]);
        $password = $_POST["password"];
        $id = $_POST["id"];
        
                 
        // validations
        $required_fields = array("username", "password");
        validate_presences($required_fields);
        // здесь $errors[] - global
    
        $fields_with_max_lengths = array("password" => 30);
        validate_max_lengths($fields_with_max_lengths);
        // здесь $errors[] - global
    
        if (!empty($errors)) {
            $_SESSION["errors"] = $errors;
            redirect_to("edit_admin.php");
        }
        
        $query  = "UPDATE admins SET ";
        $query .= " username = '{$username}', ";
        $query .= " hashed_password = '{$password}' ";
        $query .= " WHERE id = {$id} ";
        $query .= " LIMIT 1";

        $result = mysqli_query($connection, $query);

        //echo $query; // But It Will cause - Cannot modify header

        if ($result && mysqli_affected_rows($connection) >= 0) {
            // Success
            $_SESSION["message"] = "Admin updated.";
            redirect_to("manage_admins.php");
        } else {
            // Failure
            $message = "Admin update failed.";
        }
                    
    } else {
        // Вероятно, GET запрос
        // 
    }

?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
    </div>

    <div id="page">
        <?php echo message();   ?>   
        <?php $errors = errors(); ?>
		<?php echo form_errors($errors); ?>

        <h2>Edit Admin</h2>

        <form action="edit_admin.php" method="post">       
            Username:       <input type="text"     name="username" value="<?php  if (isset($admin)) { echo $admin["username"]; }  ?>" /> <br><br>     
            Password:&nbsp; <input type="password" name="password" value="" /> <br><br><br>
                            <input type="submit"   name="submit"   value="Edit Admin" />
                            
                            <input type="hidden" name="id" value="<?php  if (isset($admin)) { echo $admin["id"]; }  ?>"/>
        </form>
        
        <br><br>
        <a href="manage_admins.php">Cancel</a>

    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>