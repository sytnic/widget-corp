<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>
<?php confirm_logged_in(); ?>

<?php
if (isset($_POST['submit'])) {
	// Process the form
             
    // validations
	$required_fields = array("username", "password");
	validate_presences($required_fields);
	// здесь $errors[] - global

	$fields_with_max_lengths = array("username" => 30);
	validate_max_lengths($fields_with_max_lengths);
	// здесь $errors[] - global
   
    
    if (empty($errors)) {

		$username = mysql_prep($_POST["username"]);
	    $hashed_password = password_encrypt($_POST["password"]);
       
		$query  = "INSERT INTO admins (";
		$query .= "  username, hashed_password ";
		$query .= ") VALUES (";
		$query .= "  '{$username}', '{$hashed_password}' ";
		$query .= ")";
		$result = mysqli_query($connection, $query);

		// echo $query; // But It Will cause - Cannot modify header

		if ($result) {
			// Success
			$_SESSION["message"] = "Admin created.";
			redirect_to("manage_admins.php");
		} else {
			// Failure
			$_SESSION["message"] = "Admin's creation failed.";
			redirect_to("new_admin.php");
		}    
			
	} else {
		// Вероятно, GET запрос
		// 
	}	
}
?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
    </div>

    <div id="page">
        <?php echo message();   ?>

		<?php echo form_errors($errors); ?>  

        <h2>Create Admin</h2>

        <form action="new_admin.php" method="post">       
            Username:       <input type="text"     name="username" value=""><br><br>     
            Password:&nbsp; <input type="password" name="password" value=""><br><br><br>
                            <input type="submit"   name="submit"   value="Create Admin">
        </form>
        
        <br><br>
        <a href="manage_admins.php">Cancel</a>

    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>