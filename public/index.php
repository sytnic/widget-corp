<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php $layout_context = "public"; ?>
<?php include("../includes/layouts/header.php"); ?>

<?php // page or subject 
        find_selected_page(true); // true - публичный контент ?>

<div id="main">
  <div id="navigation">    
    <?php         
	// навигация в аргументах получает null 
    // или ассоц. массив для одного из двух аргументов, 
    // заполненный из БД, на основе GET['id'] из url 
	echo public_navigation($current_subject, $current_page); ?>	
  </div>
  <div id="page">
	
	<?php if ($current_page) {  ?>
    
    <h2><?php echo htmlentities($current_page["menu_name"])  ; ?></h2>
	<?php echo nl2br(htmlentities($current_page["content"])) ; ?>		
              
	<?php } else { ?>
            <p>Welcome!</p>
    <?php } ?>	
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>