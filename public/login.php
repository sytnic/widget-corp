<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>
<?php
$username = "";  // дефолтное значение для формы

if (isset($_POST['submit'])) {
	// Process the form
             
    // validations
	$required_fields = array("username", "password");
	validate_presences($required_fields);
	// здесь $errors[] - global
   
    
    if (empty($errors)) {
        // Attempt login

        $username = $_POST["username"];
        $password = $_POST["password"];

		// вернёт массив, строку админа из БД
		$found_admin = attempt_login($username, $password);
		
		if ($found_admin) {
			// Success
			// Mark user as logged in
			
			// Можно пометить юзера в куки
			//$_COOKIE["admin_id"] = $found_admin["id"];
			// но лучше в сессии
			$_SESSION["admin_id"] = $found_admin["id"];
			$_SESSION["username"] = $found_admin["username"];
			
			redirect_to("admin.php");
		} else {
			// Failure
			$_SESSION["message"] = "Username/password not found.";
		}    
			
	} else {
		// Вероятно, GET запрос
		// 
	}	
}
?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
    </div>

    <div id="page">
        <?php echo message();   ?>

		<?php echo form_errors($errors); ?>  

        <h2>Login</h2>

        <form action="login.php" method="post">       
            Username:       <input type="text"     name="username" value="<?php echo htmlentities($username); ?>"><br><br>     
            Password:&nbsp; <input type="password" name="password" value=""><br><br><br>
                            <input type="submit"   name="submit"   value="Submit">
        </form>

    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>