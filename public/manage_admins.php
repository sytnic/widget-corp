<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php confirm_logged_in(); ?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
    <br />
		<a href="admin.php">&laquo; Main menu</a><br />
    </div>

    <div id="page">
    <?php echo message();   ?> 
    
        <h2>Manage Admins</h2>

        <table style="width: 300px;">
            <tr  style="text-align: left;">
                <th>Username</th>
                <th>Actions</th>
            </tr>
            <!-- 
            <tr>
                <td>Jill</td>
                <td>Smith</td>
            </tr>
            -->

            <?php 
                $admin_set = find_all_admins();

                while($admin = mysqli_fetch_assoc($admin_set)) {
                  
                  //var_dump($admin);

                  $output = "<tr>";

                  $output.= "<td>";
                  $output.= htmlentities($admin["username"]);
                  $output.= "</td>";

                  $output.= "<td>";

                  $output.= '<a href="edit_admin.php?id=';
                  $output.= urlencode($admin['id']);
                  $output.= '">';
                  $output.= 'Edit</a> ';

                  $output.= '<a href="delete_admin.php?id=';
                  $output.= urlencode($admin['id']);                  
                  $output.= '"';  // закрыли кавычку после id
                  $output.= ' onclick="return confirm';
                  $output.= "('Are you sure to delete admin?');";
                  $output.= '"';  // закрыли кавычку после return confirm
                  $output.= '>';  // закрыли открывающий тег <a>
                  $output.= 'Delete</a>';

                  $output.= "</td>";

                  $output.= "</tr>";
                  echo $output;  
                }
            ?>
        </table>

        <br> 
        <a href="new_admin.php">Add new admin</a>

        <hr>
        <?php 
/*
        $password = "secret";
        $hash_format = "$2y$10$";  // 2y - blowfish, 10 - параметр стоимости (повторяемости)
        $salt = "Salt22CharactersOrMore";
        echo "Lenght: ".strlen($salt);

        $format_and_salt = $hash_format . $salt;
        $hash = crypt($password, $format_and_salt);
        echo "<br>";
        echo $hash;

        $hash2 = crypt("secret", $hash);
        echo "<br>";
        echo $hash2;
*/
        ?>

    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>