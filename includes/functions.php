<?php
    
	function redirect_to($new_location) {
	  header("Location: " . $new_location);
	  exit;
	}
	
	function mysql_prep($string) {
		global $connection;
		
		$escaped_string = mysqli_real_escape_string($connection, $string);
		return $escaped_string;
	}

	function confirm_query($result_set) {
		if (!$result_set) {
			die("Database query failed. Function confirm_query() failed, or its using in another func-s failed.");
		}
	}
    
    function form_errors($errors=array()) {
		$output = "";
		if (!empty($errors)) {
		  $output .= "<div class=\"error\">";
		  $output .= "Please fix the following errors:";
		  $output .= "<ul>";
		  foreach ($errors as $key => $error) {
		    $output .= "<li>";
            $output .= htmlentities($error);
            $output .= "</li>";
		  }
		  $output .= "</ul>";
		  $output .= "</div>";
		}
		return $output;
	}
	
	function find_all_subjects($public=true) {
		global $connection;
		
		$query  = "SELECT * ";
		$query .= " FROM subjects ";
          if($public) {
		    $query .= " WHERE visible = 1 ";
          }
        $query .= " ORDER BY position ASC";
		$subject_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($subject_set);
		
		return $subject_set;
	}
    
    function find_all_pages_for_subject($subject_id) {
		global $connection;
		
		$query  = "SELECT * ";
		$query .= " FROM pages ";
        $query .= " WHERE subject_id = {$subject_id} ";
		//$query .= " AND visible = 1 ";
		$query .= " ORDER BY position ASC";
		$pages_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($pages_set);
		
		return $pages_set;
	}
	
    // поиск по умолчанию для публичной части
	function find_pages_for_subject($subject_id, $public=true) {
		global $connection;
		
		$safe_subject_id = mysqli_real_escape_string($connection, $subject_id);
		
		$query  = "SELECT * ";
		$query .= " FROM pages ";
        $query .= " WHERE subject_id = {$safe_subject_id} ";
        if($public) {
		   $query .= " AND visible = 1 ";
		}
		$query .= " ORDER BY position ASC";
		$page_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($page_set); 

        return $page_set;		
	}
    
    // array of 1 row
    // по умолчанию публичная часть, public=true, где visible = 1
    function find_subject_by_id($subject_id, $public=true) {
		global $connection;
		
		$safe_subject_id = mysqli_real_escape_string($connection, $subject_id);
		
		$query  = "SELECT * ";
		$query .= " FROM subjects ";
		$query .= " WHERE id = {$safe_subject_id} ";
        if($public){
            $query .= " AND visible = 1 ";
        }
		$query .= " LIMIT 1";
		$subject_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($subject_set);
		
		if ($subject = mysqli_fetch_assoc($subject_set)) {
		    return $subject;
        } else {
            return null;
        }			
	}
	
    // по умолчанию публичная часть, public=true, где visible = 1
	function find_page_by_id($page_id, $public=true) {
		global $connection;
		
		$safe_page_id = mysqli_real_escape_string($connection, $page_id);
		
		$query  = "SELECT * ";
		$query .= " FROM pages ";
		$query .= " WHERE id = {$safe_page_id} ";
        if($public){
            $query .= " AND visible = 1 ";
        }
		$query .= " LIMIT 1";
		$page_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($page_set);
		
		if ($page = mysqli_fetch_assoc($page_set)) {
		    return $page;
        } else {
            return null;
        }			
	}
    
    // функция только для публичной части
    function find_default_page_for_subject($subject_id) {
		$page_set = find_pages_for_subject($subject_id);
          // второй аргумент $public=true пропущен,
          // т.к. нужен поиск по умолчанию для публичной части
		if($first_page = mysqli_fetch_assoc($page_set)) {
			return $first_page;
		} else {
			return null;
		}
	}
	
    // по умолчанию false = не публичный контент
	function find_selected_page($public=false) {
		global $current_subject; //it will be assoc.array
		global $current_page;    //it will be assoc.array
		
		if (isset($_GET["subject"])) {		
			$current_subject = find_subject_by_id($_GET["subject"], $public);
              // передано по умолчанию false, не публичный контент,
              // может передаваться true в аргументе этой функции find_selected_page()              
            if ($current_subject && $public) { 
               // если есть заданный субъект и мы в публичной части public = true, 
               // то ищем соответствующую первую подстраницу для субъекта
			  $current_page = find_default_page_for_subject($current_subject["id"]);
            } else {  // иначе (в админ части), при Get-субъекте не ищем ничего,
                      // ставим текущей странице null
              $current_page = null;
            }
		} elseif (isset($_GET["page"])) {		
			$current_page = find_page_by_id($_GET["page"], $public);
                // по умолчанию передаётся false как непубличный контент,    
                // тогда отображаются visible = 1 и 0,
                // иначе public=true, отображаются visible = 1
			$current_subject = null;
		} else {		
			$current_page = null;
			$current_subject = null;
		}
	}

    // это админ навигация
    // navigation takes 2 arguments
	// - the current subject array or null
	// - the current page array or null
    function navigation($subject_array, $page_array) {
        $output = "<ul class=\"subjects\">";
        $subject_set = find_all_subjects(false);
            // true означает WHERE visible = 1   
        while($subject = mysqli_fetch_assoc($subject_set)) {
		// output data from each row
        // instead <li> 
	      $output .= "<li" ;
            if ($subject_array && $subject["id"] == $subject_array["id"]) { 	     
	          $output .= " class=\"selected\" " ;
		    }
	      $output .= ">" ; 
	    $output .= "<a href=\"manage_content.php?subject=";
        $output .= urlencode($subject["id"]); 
        $output .= " \" >"; 
        $output .= htmlentities($subject["menu_name"]); 
        $output .= "</a>";
        
	        $page_set = find_pages_for_subject($subject["id"], false);
                 // true означает WHERE visible = 1            
	        $output .= "<ul class=\"pages\">";			
			while($page = mysqli_fetch_assoc($page_set)) {
				// output data from each row			
			    // instead <li> 
				$output .= "<li" ;
					if ($page_array && $page["id"] == $page_array["id"]) { 	     
					  $output .= " class=\"selected\" " ;
					}
				$output .= ">" ; 			
                $output .= "<a href=\"manage_content.php?page="; 
                $output .= urlencode($page["id"]); 
                $output .= "\">";
                $output .= htmlentities($page["menu_name"]); 
                $output .= "</a>";
                $output .= "</li>";		
		    } 
            mysqli_free_result($page_set); 
		  $output .= "</ul>";	
	      $output .= "</li>";
        }
      mysqli_free_result($subject_set);
      $output .= "</ul>";
    
      return $output;
    }
    
        
    // subject_array и page_array - это массивы, получаемые на основе url-строки методом GET и
    // заполняющиеся данными из базы данных
    // один или оба могут быть null    
    function public_navigation_v1($subject_array, $page_array) {
        $output = "<ul class=\"subjects\">";
        $subject_set = find_all_subjects();
     // список из subject'ов (заголовков) будет всегда         
        while($subject = mysqli_fetch_assoc($subject_set)) {
		// output data from each row
        // instead <li> 
	      $output .= "<li" ;
            if ($subject_array && $subject["id"] == $subject_array["id"]) { 	     
	          $output .= " class=\"selected\" " ;
        // selected относится только к классу       
		    }
	      $output .= ">" ; 
	    $output .= "<a href=\"index.php?subject=";
        $output .= urlencode($subject["id"]); 
        $output .= " \" >"; 
        $output .= htmlentities($subject["menu_name"]); 
        $output .= "</a>";
     // если в url-строке был задан ?subject=<id> и 
     // перебираемый сейчас в цикле while subject["id"] (а перебираются все подряд subject'ы из базы данных)
     // равен айдишнику из url
     // то выводим список подчиненных страниц текущему ?subject=<id>
        if ($subject_array && $subject["id"] == $subject_array["id"]) 
        {
	        $page_set = find_pages_for_subject($subject["id"]);	
	        $output .= "<ul class=\"pages\">";			
			while($page = mysqli_fetch_assoc($page_set)) {
				// output data from each row			
			    // instead <li> 
				$output .= "<li" ;
					if ($page_array && $page["id"] == $page_array["id"]) { 	     
					  $output .= " class=\"selected\" " ;
					}
				$output .= ">" ; 			
                $output .= "<a href=\"index.php?page="; 
                $output .= urlencode($page["id"]); 
                $output .= "\">";
                $output .= htmlentities($page["menu_name"]); 
                $output .= "</a>";
                $output .= "</li>";	
		    } 
            $output .= "</ul>";	
            mysqli_free_result($page_set); 
        }
     // или если
     // в url-строке был задан ?page=<id> и 
     // главенствующий над этим id subject
     // ( выясняется из поля page_array["subject_id"] (это связь со своим subject'ом из БД) )
     // равен перебираемому сейчас в цикле while subject'у["id"] (а в while перебираются все подряд subject'ы из БД), 
     // то выводим список страниц под текущий subject["id"]
     // согласно $page_set = find_pages_for_subject($subject["id"]);
        elseif ($page_array && $page_array["subject_id"] == $subject["id"] )
        {
	        $page_set = find_pages_for_subject($subject["id"]);	
	        $output .= "<ul class=\"pages\">";			
			while($page = mysqli_fetch_assoc($page_set)) {               
				// output data from each row			
			    // instead <li> 
				$output .= "<li" ;
					if ($page_array && $page["id"] == $page_array["id"]) { 	     
					  $output .= " class=\"selected\" " ;
					}
				$output .= ">" ; 			
                $output .= "<a href=\"index.php?page="; 
                $output .= urlencode($page["id"]); 
                $output .= "\">";
                $output .= htmlentities($page["menu_name"]); 
                $output .= "</a>";
                $output .= "</li>";               
		    } 
            $output .= "</ul>";	
            mysqli_free_result($page_set); 
        }		
	  $output .= "</li>";  // end of subject <li>
        }
        mysqli_free_result($subject_set);
     $output .= "</ul>";
    
     return $output;
    }
    
    
    // попытка рефакторинга public_navigation_v1
    
    // на самом деле в отличие от oem-оригинала 
    // из-за чувствительности предупреждений Notice
    // нужно было всего лишь добавить 
    // if ( ( $subject_array && ... и || ( $page_array && ... ) )
    // в условие вывода списка подчиненных страниц,
    // т.е. обращаться к этим массивам тогда, когда они не пустые (если они есть)
	
    // subject_array и page_array основываются на url-строке и
    // заполняются как массивы на основе соответсвующей записи из БД;
    // как минимум один всегда null (т.к. в url задаётся либо ?subject= либо ?page=)
    // или оба могут быть null
    
	function public_navigation($subject_array, $page_array) {
        $output = "<ul class=\"subjects\">";
        $subject_set = find_all_subjects();
     // в навигации список из существующих subject'ов (заголовков) будет всегда         
        while($subject = mysqli_fetch_assoc($subject_set)) {
	      $output .= "<li" ;
            if ($subject_array && $subject["id"] == $subject_array["id"]) { 	     
	          $output .= " class=\"selected\" " ;     
		    }
	      $output .= ">" ; 
	    $output .= "<a href=\"index.php?subject=";
        $output .= urlencode($subject["id"]); 
        $output .= " \" >"; 
        $output .= htmlentities($subject["menu_name"]); 
        $output .= "</a>";
    
    
     // если в url-строке был задан ?subject=<id> и 
     // перебираемый сейчас в цикле while subject["id"] (а перебираются все подряд subject'ы из базы данных)
     // равен айдишнику из url
     // то выводим список подчиненных страниц текущему ?subject=<id>
    
     // или если
     // в url-строке был задан ?page=<id> и 
     // главенствующий над этим id subject
     // ( выясняется из поля page_array["subject_id"] (это связь со своим subject'ом из БД) )
     // равен перебираемому сейчас в цикле while subject'у["id"] (а в while перебираются все подряд subject'ы из БД), 
     // то выводим список страниц под текущий subject["id"]
     // согласно $page_set = find_pages_for_subject($subject["id"]);
    
            if (
                 ( $subject_array && $subject["id"] == $subject_array["id"] ) 
               ||
                 ( $page_array && $page_array["subject_id"] == $subject["id"] )
               )
            
            {
                $page_set = find_pages_for_subject($subject["id"]);	
                $output .= "<ul class=\"pages\">";			
                while($page = mysqli_fetch_assoc($page_set)) {
                    $output .= "<li" ;
                        if ($page_array && $page["id"] == $page_array["id"]) { 	     
                          $output .= " class=\"selected\" " ;
                        }
                    $output .= ">" ; 			
                    $output .= "<a href=\"index.php?page="; 
                    $output .= urlencode($page["id"]); 
                    $output .= "\">";
                    $output .= htmlentities($page["menu_name"]); 
                    $output .= "</a>";
                    $output .= "</li>";	
                } 
                $output .= "</ul>";	
                mysqli_free_result($page_set); 
            }   
        
	      $output .= "</li>";  // end of subject <li>
        }
        mysqli_free_result($subject_set);
     $output .= "</ul>";
    
     return $output;
    }

    function find_all_admins() {
        global $connection;
		
		$query  = "SELECT * ";
		$query .= " FROM admins ";
        $query .= " ORDER BY username ASC";
		$admin_set = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($admin_set);
		
		return $admin_set;
    }

    function find_admin_by_id($admin_id) {
        global $connection;
		
		$safe_admin_id = mysqli_real_escape_string($connection, $admin_id);
		
		$query  = "SELECT * ";
		$query .= " FROM admins ";
		$query .= " WHERE id = {$safe_admin_id} ";
		$query .= " LIMIT 1";
		$admin_set_row = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($admin_set_row);
		
		if ($admin_row = mysqli_fetch_assoc($admin_set_row)) {
		    return $admin_row;
        } else {
            return null;
        }		
    }

	function find_admin_by_username($username) {
        global $connection;
		
		$safe_username = mysqli_real_escape_string($connection, $username);
		
		$query  = "SELECT * ";
		$query .= " FROM admins ";
		$query .= " WHERE username = '{$safe_username}' ";
		$query .= " LIMIT 1";
		$admin_set_row = mysqli_query($connection, $query);
		// Test if there was a query error
		confirm_query($admin_set_row);
		
		if ($admin_row = mysqli_fetch_assoc($admin_set_row)) {
		    return $admin_row;
        } else {
            return null;
        }		
    }

    // аналог встроенной php-функции password_hash() 
    function password_encrypt($password) {
        $hash_format = "$2y$10$";           // Tells PHP to use Blowfish with a "cost" of 10
	    $salt_length = 22; 					// Blowfish salts should be 22-characters or more
	    $salt = generate_salt($salt_length);
	    $format_and_salt = $hash_format . $salt;
	    $hash = crypt($password, $format_and_salt);
		return $hash;
	}
	
	function generate_salt($length) {
	    // Генерируется любая случайная строка
        // Not 100% unique, not 100% random, but good enough for a salt
	    // MD5 returns 32 characters
	  $unique_random_string = md5(uniqid(mt_rand(), true));
	  
        // Довести строку до приемлемой соли в Блоуфиш
		// Valid characters for a salt are [a-zA-Z0-9./]
        // https://www.php.net/manual/ru/function.crypt.php  "./0-9A-Za-z"
	  $base64_string = base64_encode($unique_random_string);
	  
        // Заменить созданные base64_encode плюсы в соли на точки для приемлемой соли согласно "./0-9A-Za-z"
		// But not '+' which is valid in base64 encoding
	  $modified_base64_string = str_replace('+', '.', $base64_string);
	  
        // Обрезать строку для приемлемой длины соли в Блоуфиш (22 символа или больше)
		// Truncate string to the correct length
	  $salt = substr($modified_base64_string, 0, $length);
	  
        // Возвращается случайная соль, приемлемая для Блоуфиш
		return $salt;
	}
	
    // аналог встроенной php-функции password_verify() 
	function password_check($password, $existing_hash) {
		  // existing hash contains format and salt at start
	    $hash = crypt($password, $existing_hash);
	    if ($hash === $existing_hash) {
	      return true;
	    } else {
	      return false;
	    }
	}

	/**
	 *  вернёт массив "строку админа" из БД ($admin_row)
	 */	 
	function attempt_login($username, $password) {
		$admin = find_admin_by_username($username);
		// вернётся $admin_row

		// если $admin_row есть
		if ($admin) {
			// admin found, now check password
            if (password_check($password, $admin["hashed_password"])) {
				// password matches
				return $admin;
			} else {
				// password does not match
				return false;
			}
		} else {
			// admin not found
			return false;
		}

	}
    
	/**
	 * return T/F
	 */
	function logged_in() {
		return isset($_SESSION["admin_id"]);
	}

	function confirm_logged_in() {
		if(!logged_in()){
			redirect_to("login.php");
		}
	}
    
?>